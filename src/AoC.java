

public class AoC {
    public static void main(String[] args) {
        new Day00().run(true);
        new Day01().run(true);
        new Day02().run(true);
    }
}
