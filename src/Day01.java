
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;


// note the class name and the first method are the same, its a constructor
public class Day01 {
    // get the name of teh class
    private final String name = this.getClass().getSimpleName();

    // these time teh individual parts
    private long time_generator;
    private long time_part1;
    private long time_part2;

    // input is the data, processed is it made into a format we can use
    private String input;
    private ArrayList<Integer> processed;

    private int part1;
    private int part2;

    public void run(boolean test) {
        if (test){
            test();
        }
        // test first


        get_file();
        process_input();
        part1();
        part2();

        // print out teh results
        System.out.printf(
            """
            %s
                Processing (%d ms)
                Part 1     (%d ms): %d
                Part 2     (%d ms): %d
                
            """,
            this.name,
            this.time_generator,
            this.time_part1, this.part1,
            this.time_part2, this.part2
        );
    }

    // read the file to a string
    private void get_file(){
        String file_path = "./inputs/" + this.name + ".txt";

        try {
            this.input = Files.readString(Path.of(file_path));
        } catch (IOException e) {
            System.err.println("No File found: "+ file_path);
            e.printStackTrace();
        }
    }


    private void test() {
        // pop teh test data here
        this.input = """
                199
                200
                208
                210
                200
                207
                240
                269
                260
                263
                """;

        // runs teh above input against the functions
        process_input();
        part1();
        part2();


        // AoC will tell you what teh answer to the test data is, use that here
        int part1 = 7;
        int part2 = 5;


        if (this.part1 != part1) {
            System.err.println("TEST: Part 1 is incorrect: Left: " + this.part1 + " Right: " + part1);
        }
        if (this.part2 != part2) {
            System.err.println("TEST: Part 2 is incorrect: Left: " + this.part2 + " Right: " + part2);
        }
    }


    // process the input into something that is easy to use
    private void process_input (){
        Instant start = Instant.now();
        /* *******************************************/

        // create the array its going to go into
        this.processed = new ArrayList<>();

        // split it up to get each line
        String[] lines = this.input.split("\\r?\\n");

        // for line in lines
        for (String line: lines){
            // example only has one number a line, but you may need to split it again if there are multiple numbers on each line
            // you can have arrays within arrays to create 2D/3D arrays

            try{
                Integer number = Integer.valueOf(line);
                this.processed.add(number);
            }
            catch (NumberFormatException ex){
                ex.printStackTrace();
            }
        }


        /* *******************************************/
        Instant finish = Instant.now();
        this.time_generator = Duration.between(start, finish).toMillis();
    }


    // part 1, the input is passed to it
    // return type is int as that is what most of the AoC answers are
    private void part1(){
        Instant start = Instant.now();
        /* *******************************************/

        int count = 0;
        for(int i=1; i< this.processed.size(); i++){
            int last = this.processed.get(i-1);
            int current = this.processed.get(i);

            if(current > last){
                count += 1;
            }
        }

        this.part1 = count;

        /* *******************************************/
        Instant finish = Instant.now();
        this.time_part1 = Duration.between(start, finish).toMillis();
    }

    private void part2(){
        Instant start = Instant.now();
        /* *******************************************/

        int count = 0;
        for(int i=3; i< this.processed.size(); i++){
            int last = this.processed.get(i -1) + this.processed.get(i-2) + this.processed.get(i - 3);
            int current = this.processed.get(i) + this.processed.get(i-1) + this.processed.get(i - 2);

            if(current > last){
                count += 1;
            }
        }

        this.part2 = count;

        /* *******************************************/
        Instant finish = Instant.now();
        this.time_part2 = Duration.between(start, finish).toMillis();
    }
}
