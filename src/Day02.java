
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Objects;


// note the class name and the first method are the same, its a constructor
public class Day02 {
    // get the name of teh class
    private final String name = this.getClass().getSimpleName();

    // these time teh individual parts
    private long time_generator;
    private long time_part1;
    private long time_part2;

    // input is the data, processed is it made into a format we can use
    private String input;
    private ArrayList<String[]> processed;

    private int part1;
    private int part2;

    public void run(boolean test) {
        if (test){
            test();
        }
        // test first


        get_file();
        process_input();
        part1();
        part2();

        // print out teh results
        System.out.printf(
            """
            %s
                Processing (%d ms)
                Part 1     (%d ms): %d
                Part 2     (%d ms): %d
                
            """,
            this.name,
            this.time_generator,
            this.time_part1, this.part1,
            this.time_part2, this.part2
        );
    }

    // read the file to a string
    private void get_file(){
        String file_path = "./inputs/" + this.name + ".txt";

        try {
            this.input = Files.readString(Path.of(file_path));
        } catch (IOException e) {
            System.err.println("No File found: "+ file_path);
            e.printStackTrace();
        }
    }


    private void test() {
        // pop teh test data here
        this.input = """
                forward 5
                down 5
                forward 8
                up 3
                down 8
                forward 2
                """;

        // runs teh above input against the functions
        process_input();
        part1();
        part2();


        // AoC will tell you what teh answer to the test data is, use that here
        int part1 = 150;
        int part2 = 900;


        if (this.part1 != part1) {
            System.err.println("TEST: Part 1 is incorrect: Left: " + this.part1 + " Right: " + part1);
        }
        if (this.part2 != part2) {
            System.err.println("TEST: Part 2 is incorrect: Left: " + this.part2 + " Right: " + part2);
        }
    }


    // process the input into something that is easy to use
    private void process_input (){
        Instant start = Instant.now();
        /* *******************************************/

        // create the array its going to go into
        this.processed = new ArrayList<>();

        // split it up to get each line
        String[] lines = this.input.split("\\r?\\n");

        // for line in lines
        for (String line: lines){
            String[] broken = line.split(" ");
            this.processed.add(broken);
        }


        /* *******************************************/
        Instant finish = Instant.now();
        this.time_generator = Duration.between(start, finish).toMillis();
    }


    // part 1, the input is passed to it
    // return type is int as that is what most of the AoC answers are
    private void part1(){
        Instant start = Instant.now();
        /* *******************************************/

        int horizontal = 0;
        int depth = 0;
        for (String[] instruction: this.processed){
            String direction = instruction[0];

            int number = 0;
            try{
                number = Integer.parseInt(instruction[1]);
            }
            catch (NumberFormatException ex){
                ex.printStackTrace();
            }

            if (Objects.equals(direction, "forward")){
                horizontal += number;
            } else if (Objects.equals(direction, "down")){
                depth += number;
            } else if (Objects.equals(direction, "up")){
                depth -= number;
            }
        }

        this.part1 = horizontal * depth;

        /* *******************************************/
        Instant finish = Instant.now();
        this.time_part1 = Duration.between(start, finish).toMillis();
    }

    private void part2(){
        Instant start = Instant.now();
        /* *******************************************/

        int horizontal = 0;
        int depth = 0;
        int aim = 0;

        for (String[] instruction: this.processed) {
            String direction = instruction[0];
            int number = 0;
            try {
                number = Integer.parseInt(instruction[1]);
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
            }

            if (Objects.equals(direction, "forward")) {
                horizontal += number;
                depth += aim * number;

            } else if (Objects.equals(direction, "down")) {
                aim += number;
            } else if (Objects.equals(direction, "up")) {
                aim -= number;
            }
        }

        this.part2 = horizontal * depth;

        /* *******************************************/
        Instant finish = Instant.now();
        this.time_part2 = Duration.between(start, finish).toMillis();
    }
}
